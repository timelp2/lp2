<?php 

class Tabela {
    // atributos
    private $dados;

    // construtor
    function __construct($dados){
        $this->dados = $dados;
    }

    // métodos
    public function getHTML(){
        $html = '<table class="table">';
        $html .= $this->tableHeader();
        $html .= $this->tableBody($this->dados);
        $html .= '</table>';
        return $html;
    }
    
    private function tableBody($data){
        $cont = 1;
        $html = '<tbody>';  
        foreach ($data as $row) {
            $html .= '<tr>';
            $html .= '<th scope="row">'.($cont).'</th>';
            $html .= '<td><a href="'.base_url('cliente/detalhe/'.$cont).'">'.$row[0].'</a></td>';
            $html .= '<td>'.$row[1].'</td>';
            $html .= '<td>'.$row[2].'</td>';
            $html .= '</tr>';
            $cont++;
        }
        $html .= '</tbody>';
        return $html;
    }

    private function tableHeader(){
        $html = '<thead class="indigo darken-4 white-text">
                    <tr>
                        <th scope="col">Nº</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Sobrenome</th>
                        <th scope="col">Usuário</th>
                    </tr>
                </thead>';
        return $html;
    }
}