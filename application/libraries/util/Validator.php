<?php 

    class Validator extends CI_Object{

        public function produto_validate(){
            $this->form_validation->set_rules('nome', 'Nome do produto', 'trim|required|min_length[4]|max_length[40]');
            $this->form_validation->set_rules('descricao', 'descrição do produto', 'trim|required|min_length[10]|max_length[100]');
            $this->form_validation->set_rules('preco', 'preço do produto', 'trim|required');
            return $this->form_validation->run();
        }

        public function valida_dados_pessoais(){
            $this->form_validation->set_rules('dados[nome]', 'Nome do usuario', 'trim|required|min_length[2]|max_length[20]');
            $this->form_validation->set_rules('dados[sobrenome]', 'Sobrenome do usuario', 'trim|required|min_length[4]|max_length[80]');
            $this->form_validation->set_rules('dados[sexo]', 'Gênero do usuario', 'required|in_list[1,2]');
            $this->form_validation->set_rules('dados[email]', 'E-mail do usuario', 'trim|required|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('dados[senha]', 'Senha do usuario', 'trim|required|min_length[8]|max_length[100]');
            

        }

        public function valida_endereco(){
            $this->form_validation->set_rules('endereco[rua]','rua', 'trim|required|min_length[4]|max_length[100]');
            $this->form_validation->set_rules('endereco[bairro]','bairro', 'trim|required|min_length[4]|max_length[100]');
            $this->form_validation->set_rules('endereco[cidade]','cidade', 'trim|required|min_length[4]|max_length[100]');
            $this->form_validation->set_rules('endereco[estado]','estado', 'trim|required|min_length[4]|max_length[100]');


        }

        


    }

?>
