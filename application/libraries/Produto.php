<?php

class Produto extends CI_Object {
    
    public function get(){
        $rs = $this->db->get('produto');
        return $rs->result_array();
    }

    public function get_by_id($id){
        $cond['id'] = $id;
        $rs = $this->db->get_where('produto', $cond);
        return $rs->row_array();
    }

    public function set(Array $data){
        return $this->db->insert('produto', $data);
        
    }

    public function update($data, $id){
        
        $cond['id'] = $id;
        return $this->db->update('produto', $data, $cond);
    }

    public function delete($id){
        $cond = array('id' => $id);
        return $this->db->delete('produto', $cond);
    }

    public function getAll(){
        $sql = "SELECT nome, descricao, preco, id FROM Produto";
        $rs = $this->db->query($sql);
        return $rs->result_array();

    }

}
