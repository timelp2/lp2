<?php 

class DadosPessoais extends CI_Object{

    public function insere($data){
        $this->db->insert('DadosPessoais', $data);
        return $this->db->insert_id();
    }

    public function get($id){
        $c['id'] = $id;
        $rs = $this->db->get_where('DadosPessoais', $c);
        echo $this->db->last_query();
        return $rs->row_array();

        //return $rs->result_array()[0];

    }

    public function getAll(){
        $sql = "SELECT nome, sobrenome, email, IF(sexo = 1, 'feminino', 'masculino') AS genero, id FROM DadosPessoais";
        $rs = $this->db->query($sql);
        return $rs->result_array();

    }



}