<?php

class Endereco extends CI_Object{

    function insere($data, $id){
        $data['id_usuario'] = $id;
        $this->db->insert('endereco', $data);
    }

    public function get( $id){
        $c['id_usuario'] = $id;
        $rs = $this->db->get_where('endereco', $c);
        return $rs->row_array();

    }

}