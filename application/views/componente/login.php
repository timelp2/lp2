<br>
<div class="container mt-5"><br>
    <?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <form method="POST" action="<?php echo base_url();?>index.php/Loja/admin"  class="text-center border border-light p-5">
                <p class="h4 mb-4">Login</p>
                <input value="<?= set_value('dados[email]') ?>"  type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">
           
                <input value="<?= set_value('dados[senha]') ?>" type="password" id="senha" name="senha" class="form-control" placeholder="Senha">
            
                <button class="btn btn-dark my-4 btn-block" type="submit">Entrar</button>
                <a class="btn btn-dark my-4 btn-block" href="http://localhost/lp2/lp2/index.php/Loja/Cadastro">Cadastre-se</a>
            </form>
        </div>
    </div>
</div>