<br><br>
<div class="container mt-5">
    <div class="row">
        <div class="col-md-20 mx-auto">
            <div class="quemsomos">
                <div class="quemsomos-body">
                <hr><h4 class="quemsomos-title"><a>Quem Somos </a></h4> </hr>
                <hr></hr>
                    <p class="quemsomos-text">
                    <h5>
                    Ser smart é respirar moda e tecnologia e disso a Yelkao entende: 
                    somos o maior e-commerce de moda e lifestyle da América Latina, 
                    com mais de 5 anos de existência, plataforma universal mobile 
                    (sim, você pode acessar por tablets e smartphones IOS e Android! \o/) 
                    e filiais na Argentina, Chile e Colômbia.
                    Na sua smartfashion você encontra tudo o que precisa com poucos cliques, 
                    sem julgamentos e ainda recebe rapidinho em todo o Brasil! Só 
                    no país são mais de mil marcas e 110 mil produtos divididos 
                    em seis categorias: roupas, sapatos, acessórios, beleza, 
                    cama/mesa/banho e decoração.
                    Antes de comprar, fique por dentro de algumas dicas: 
                    nossos estoques são limitados e, em caso de divergência, 
                    o que vale é o preço da sacola; os valores podem ser 
                    alterados sem aviso prévio e as cores podem variar de acordo 
                    com as configurações do seu monitor. Tudo certo? Então venha 
                    pra Yelkao. Sua smartfashion.</h5></p>
                    
                </div>
            </div>
        </div>
    </div>
</div>