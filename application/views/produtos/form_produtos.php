
<br>
<br>
<div class="container mt-5">
<?= validation_errors('<div class="alert alert-danger">', '</div>'); ?>
    <div class="row">
        <div class="col-md-6 mx-auto">
            <form method="POST" class="text-center border border-light p-5">

                <p class="h4 mb-4">Cadastro de Produtos</p>
                
                <!-- Nome do Produto -->
                <input type="text" value="<?= set_value('nome') ?>" id="nome" name="nome" class="form-control mb-4" placeholder="Nome do Produto">

                <!-- Descricao -->
                <input type="text" value="<?= set_value('descricao')  ?>" id="descricao" name="descricao" class="form-control mb-4" placeholder="Descricao">

                <!-- Preço -->
                <input type="text" value="<?= set_value('preco')  ?>" id="preco" name="preco" class="form-control mb-4" placeholder="Preço">

                <!-- Sign in button -->
                <button class="btn btn-info btn-block my-4" type="submit">Salvar</button>
                

            </form>

        </div>
    </div>
</div>
