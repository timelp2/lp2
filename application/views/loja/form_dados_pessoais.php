<div class="container mb-5">
    <div class="row">
    
        <div class="col-md-6 mx-auto">
            <p class="h4 mb-4">Cadastre-se</p>
            <input  value="<?= set_value('dados[nome]') ?>" name="dados[nome]" type="text" class="form-control mb-4" placeholder="Nome">
            <input  value="<?= set_value('dados[sobrenome]') ?>" name="dados[sobrenome]" type="text" class="form-control mb-4" placeholder="Sobrenome">

            <select name="dados[sexo]" class="browser-default custom-select mb-4">
                <option value="0">Sexo</option>
                <option value="1" <?= set_value('dados[sexo]') == 1 ? 'selected' : '' ?> >Feminino</option>
                <option value="2" <?= set_value('dados[sexo]')  == 2 ? 'selected' : ''  ?> >Masculino</option>
            </select>
            <input value="<?= set_value('dados[email]') ?>" type="email" name="dados[email]" class="form-control mb-4" placeholder="E-mail" /><br/>
            <input value="<?= set_value('dados[senha]') ?>" type="password" name="dados[senha]" class="form-control mb-4" placeholder="Senha" /><br/>
        </div>
    </div>
</div>