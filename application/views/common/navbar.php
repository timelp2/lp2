
  <!-- Navbar -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
 
    <div class="container">
      <!-- Brand -->
      <a class="navbar-brand waves-effect" href="<?php echo base_url();?>/Cliente">
        
      </a>

      <!-- Collapse -->
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <!-- Links -->
      <div class="collapse navbar-collapse" id="navbarSupportedContent">

        <!-- Left -->
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link waves-effect" href="<?php echo base_url();?>index.php/Loja">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="<?php echo base_url();?>index.php/Loja/Produtos">Produtos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link waves-effect" href="<?php echo base_url();?>index.php/Loja/Contato">Contato</a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link waves-effect" href="<?php echo base_url();?>index.php/Loja/Login">Login</a>
          </li>
        </ul>
  
        <!-- Right -->
        <ul class="navbar-nav nav-flex-icons">
          <li class="nav-item">
            <a class="nav-link waves-effect">
              <span class="badge red z-depth-1 mr-1"> 1 </span>
              <i class="fas fa-shopping-cart"></i>
              <span class="clearfix d-none d-sm-inline-block"> Carrinho</span>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://www.facebook.com" class="nav-link waves-effect" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://twitter.com" class="nav-link waves-effect" target="_blank">
              <i class="fab fa-twitter"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="https://www.instagram.com" class="nav-link border-light rounded waves-effect"
              target="_blank">
              <i class="fab fa-instagram mr-2"></i>
           </a>
          </li>
        </ul>

      </div>

    </div>
  </nav>
  <!-- Navbar -->

  </br>

  </br>

