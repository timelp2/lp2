<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Yelkao.com</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="<?= base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="<?= base_url('assets/css/mdb.min.css')?>" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="<?= base_url('assets/css/style.min.css' )?>" rel="stylesheet">
  <link rel="sortcut icon" type="image/icon" href="<?= base_url('assets/img/logo.png')?>">
  
  <style type="text/css">
    html,
    body,
    header,
    .carousel {
      height: 60vh;
    }

    @media (max-width: 740px) {

    html,
    body,
    header,
    .carousel {
    height: 100vh;
    }

    }

    @media (min-width: 800px) and (max-width: 850px) {

    html,
    body,
    header,
    .carousel {
    height: 100vh;
    }

    }

  </style>
</head>

<body>
