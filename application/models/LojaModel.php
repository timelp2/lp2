<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH.'libraries/component/Table.php';

class LojaModel extends CI_Model{

    function __construct(){
        $this->load->library('util/Validator');

    }

    public function lista_produtos_lancamentos(){
        $this->load->library('Produto');
        
        $v = $this->produto->get();
        $html = $this->cards($v);
        
        return $html;
    }


    private function cards($v){
        $cont = 0;
        $html = '<div class="container">
        <div class="row wow fadeIn">';

       
                    
                        foreach ($v as $produto) {
                            
                        if($cont == 16){
                            $html .= '</div>';
                            $html .= '</div>';
                                        
                            return $html;
                        
                        }

                        $html .= '<div class="col-lg-3 col-md-6 mb-4">';
                        $html .= '<div class="card">
                        <div class="view overlay" <style = width="248px" height:250px; ></style>';
                        $html .= '<img src="../assets/img/lancamento/roupa'.$produto['id'].'.jpg" class="card-img-top" id="imagem_camisa" alt=""><a>';
                        $html .= '<div class="mask rgba-white-slight"></div></a></div>';
                        
                        $html .= '<div class="card-body text-center">';
                
                        $html .= '<a href="" class="grey-text">';
                        
                        $html .= '</a>';
                        $html .= '<h5>';
                        $html .= '<strong>';
                        $html .='<a href="" class="dark-grey-text">'.$produto['nome'].
                        '<span class="badge badge-pill danger-color">Lançamento</span>';
                        $html .= '</a>
                        </strong>
                        </h5>
                        <h4><a  href="Detalhes/'.$produto['id'] .'"class="font-weight-bold red-text">Detalhes</a></h4>
                        <h4 class="font-weight-bold blue-text">';
                        $html .= '<strong>'.$produto['preco'].'</strong>
                        </h4>
                        </div>
                        </div>
                        </div>';
                        $cont++;
                        }

        $html .= '</div>';
        $html .= '</div>';
                
        return $html;
    }

   

}