<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class ProdutosModel extends CI_Model{

    function __construct(){
        $this->load->library('util/Validator');
    }

    public function lista_todos_produtos(){
        $this->load->library('Produto');
        
        $v = $this->produto->get();
        $html = $this->produtos($v);
        
        return $html;
    }


    public function detalha($id){

        $this->load->library('Produto');

        $produto = $this->produto->get_by_id($id);

        $html = $this->detalhe_produto($produto);

        return $html;

    }

    private function validate(){
        $this->load->library('util/Validator');

        $this->validator->produto_validate();
        return $this->form_validation->run();

    }


    private function produtos($v){
        $cont = 0;
        $html = '<div class="container">
        <div class="row wow fadeIn">';

       
                    
                        foreach ($v as $produto) {
                            
                        $html .= '<div class="col-lg-3 col-md-6 mb-4">';
                        $html .= '<div class="card">
                        <div class="view overlay" <style = width="248px" height:250px; ></style>';
                        $html .= '<img src="../../assets/img/produtos/roupa'.$produto['id'].'.jpg" class="card-img-top" alt=""><a>';
                        $html .= '<div class="mask rgba-white-slight"></div></a></div>';
                        
                        $html .= '<div class="card-body text-center">';
                
                        $html .= '<a href="" class="grey-text">';
                        $html .= '</a>';
                        $html .= '<h5>';
                        $html .= '<strong>';
                        $html .='<a href="" class="dark-grey-text">'.$produto['nome'].
                        '</a>
                        </strong>
                        </h5>
                        <h4 class="font-weight-bold blue-text">';
                        $html .= '<strong>'.$produto['preco'].'</strong>
                        </h4>
                        
                        <h4><a  href="Detalhes/'.$produto['id'] .'"class="font-weight-bold red-text">Detalhes</a></h4>
                        </div>
                        </div>
                        </div>';
                        $cont++;
                        }

        $html .= '</div>';
        $html .= '</div>';
                
        return $html;
    }


    private function Detalhe_produto($produto){
        $html = '
        <main class="mt-5 pt-4">
          <div class="container dark-grey-text mt-5">
      
            <div class="row wow fadeIn">
      
      
              <div class="col-md-6 mb-4">
      
              <img src="../../../assets/img/produtos/roupa'.$produto['id'].'.jpg" class="card-img-top" alt="">
      
              </div>
      
      
      
              <div class="col-md-6 mb-4">
      
      
                <div class="p-4">';
                  $html .= '<h5>';
                  $html .= '<strong class="lead font-weight-bold">'.$produto['nome'].'</strong></h5>
                  
                  <p class="lead font-weight-bold">Descrição</p>
                  <p>'.$produto['descricao'].'</p>

                  <h4 class="font-weight-bold blue-text">';
                        $html .= '<strong>Total: $'.$produto['preco'].'</strong>
                        </h4>
      
                  <form class="d-flex justify-content-left">
      
                    <input type="number" value="1" class="form-control" style="width: 100px">
                    <button class="btn btn-primary btn-md my-0 p" type="submit">
                    Adicionar ao carrinho
                    <i class="fas fa-shopping-cart ml-1"></i>
                    </button>
      
                  </form>
      
                </div>
      
      
              </div>
      
      
            </div>
            
            </div>
        </main>';

            return $html;
    }

    public function salva(){
        if(sizeof($_POST) == 0) return;

        if($this->validator->produto_validate()){
            $data['nome'] = $this->input->post('nome');
            $data ['descricao'] = $this->input->post('descricao');
            $data ['preco'] = $this->input->post('preco');

            $this->load->library('Produto');
            $status = $this->produto->set($data);
            if($status) {
            return $html = '
            <div class="container mt-5 pt-3">
            <br/><br/>
            <h3><div class="font-weight-bold text-center blue-text"> Cadastrado com sucesso </div></h3>
            </div>
            <a href="../Loja/Admin" class="btn btn-dark btn-block my-4">Vizualizar</a>
            ';
            }else{
                return $html = '
                <div class="container mt-5 pt-3">
                <br/><br/>
                <h3><div class="font-weight-bold text-center red-text"> Erro ao cadastrar o produto</div></h3>
                </div>';
            }
            //redirect('Loja/Admin'); 
        }
        

        
    }
    

    public function tabela(){
        $this->load->library('Produto', null, 'lista');
        $labels = array('Nome', 'Descrição', 'Preço', 'Opções');
        $lista = $this->lista->getAll();
        foreach ($lista as $key => $val){
            $lista[$key]['botoes'] = $this->action_buttons($val);
            //unset($data[$key]['id']);

        }


        $table = new Table($lista, $labels);
        return $table->getHTML();

    }
            
            public function carregar($id){        
                $this->load->library('Produto');
                $_POST = $this->produto->get_by_id($id);

            }

            public function atualizar($id){
                if(sizeof($_POST) == 0) return;
    
                if($this->validator->produto_validate()){
                $data['nome'] = $this->input->post('nome');
                $data ['descricao'] = $this->input->post('descricao');
                $data ['preco'] = $this->input->post('preco');
    
                $this->load->library('Produto');
                $status = $this->produto->update($data, $id);
                if($status){
                return $html = '
                <div class="container mt-5 pt-3">
                <br/><br/>
                <h3><div class="font-weight-bold text-center blue-text"> Alterado com sucesso </div></h3>
                </div>
                <a href="../Admin" class="btn btn-dark btn-block my-4">Vizualizar</a>';
                }else{
                        return $html = '
                        <div class="container mt-5 pt-3">
                        <br/><br/>
                        <h3><div class="font-weight-bold text-center red-text"> Erro ao alterar o produto</div></h3>
                        </div>';
                        //redirect('Loja/Admin'); 
                     }
                }
            }

        public function delete($id){
            $this->load->library('Produto');
            $status = $this->produto->delete($id);
            if($status){
                return $html = '
                <div class="container mt-5 pt-3">
                <br/><br/>
                <h3><div class="font-weight-bold text-center blue-text"> Deletado com sucesso </div></h3>
                </div>
                <a href="../Admin" class="btn btn-dark btn-block my-4">Vizualizar</a>';
                }else{
                        return $html = '
                        <div class="container mt-5 pt-3">
                        <br/><br/>
                        <h3><div class="font-weight-bold text-center red-text"> Erro ao Deletar o produto</div></h3>
                        </div>';
                        //redirect('Loja/Admin'); 
                     }

        }

        private function action_buttons($row){
            $html  = '<a href="'.base_url('index.php/Loja/edit/'.$row['id']).'"><i class="fas fa-edit mr-3 blue-text" title="Editar" ></i></a>';
            $html .= '<a href="'.base_url('index.php/Loja/delete/'.$row['id']).'"><i class="fas fa-trash mr-3 red-text" title="Deletar" ></i></a>';
            $html .= '<a href="'.base_url('index.php/Loja/Insere').'"> <i class="fas fa-plus-circle green-text" title="Inserir"></i></a>';
            return $html;
    
        }





}