<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Loja extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model('LojaModel', 'loja');
        $this->load->model('CadastroModel', 'cadastro');
        $this->load->model('ProdutosModel', 'produtos');
    }

    public function index(){
        $html = $this->load->view('componente/quemsomos', null, true);
        $html .= $this->load->view('componente/cards', null, true);
        $html .= $this->loja->lista_produtos_lancamentos();
        $html .= $this->load->view('componente/paginacao', null, true);
        $this->show($html);
    }

    public function Produtos(){
        $html = $this->load->view('componente/cards', null, true);
        $html .= $this->produtos->lista_todos_produtos();
        $html .= $this->load->view('componente/paginacao', null, true);
        $this->show($html);

    }

    public function Contato(){
        $this->cadastro->salva();
        $html = $this->load->view('componente/contato', null, true);
        $this->show($html);
        
    }

    public function Login(){
        
        $html = $this->load->view('componente/login', null, true);
        $this->show($html);
    }

        public function Cadastro(){
            $this->cadastro->salva();

            $data['forms'] = $this->load->view('loja/form_dados_pessoais', null, true);
            $data['forms'] .= $this->load->view('loja/form_endereco', null, true);
            $html = $this->load->view('loja/form_base', $data, true);
            $this->show($html);
    }

    public function admin(){
    
            $data['tabela'] = $this->produtos->tabela();
            $html = $this->load->view('produtos/tabela', $data, true);
            $this->show($html);
    }

    public function Insere(){
        $html = $this->produtos->salva();
         $html .= $this->load->view('produtos/form_produtos', null, true);
        $this->show($html);
        
    }

    public function edit($id){
       $html =  $this->produtos->atualizar($id);
        $this->produtos->carregar($id);

        $html .= $this->load->view('produtos/form_produtos', null, true);
        $this->show($html);
    }

    public function delete($id){
        $html = $this->produtos->delete($id);
        $this->show($html);
        
    }

    public function detalhes($id){
        $html = $this->produtos->detalha($id);
        $this->show($html);

    }

}
